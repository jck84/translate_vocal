from time import time
import cv2
from deepface import DeepFace
from alexa import parle
import os
import time
from googletrans import Translator

translator = Translator()

def bienvenue():
    parle("Bienvenue, un instant s'il vous plait je scan votre visage")
    print("in progress...")
    cam = cv2.VideoCapture(0)
    result, image = cam.read()
    if result:
        # showing result, it take frame name and image 
        # output
        # saving image in local storage
        cv2.imwrite("visage.png", image)
        # If keyboard interrupt occurs, destroy image 
        # window
        cv2.waitKey(0)
    # If captured image is corrupted, moving to else part
    else:
        print("No image detected. Please! try again")
    cam.release()
    obj = DeepFace.analyze(img_path = "/home/blurp/Bureau/python_docs/translate_vocal/translate_vocal/visage.png", 
            actions = ['age', 'gender', 'race', 'emotion'])
    listee = []
    for key, value in obj.items():
        if key == "age":
            listee.append(str(value))
        if key == "gender":
            listee.append(str(value))
        if key == "dominant_race":
            listee.append(str(value))
        if key == "dominant_emotion":
            listee.append(str(value))
    print(listee)
    if listee[2] == "white":
        listee[2] = "caucasian"

    age = translation(listee[0])
    genre = translation(listee[1])
    emotion = translation(listee[3])
    race = translation(listee[2])

    parle("vous avez environ "+(age)+" ans"+ ", vous êtes de genre : "+(genre)+ ", votre race dominante est "+(race)+ ", votre émotion actuelle est "+(emotion))
    time.sleep(1)
    os.remove("/home/blurp/Bureau/python_docs/translate_vocal/translate_vocal/visage.png")

def translation(mot):
    transl = translator.translate(mot, dest='fr')
    result = transl.text
    return result

if __name__ == "__main__":
    bienvenue()