from googletrans import Translator
import speech_recognition as sr
from pydub import AudioSegment
import os
from playsound import playsound
import time
from gtts import gTTS
from tkinter import *

root = Tk()
root.title('Traducteur vocal')
root.geometry('600x590')
frame = LabelFrame(root, text='utilisation', padx=30, pady=30)
frame.grid(padx=50, pady=50)

translator = Translator()


def fr_en():
    recognizer = sr.Recognizer()
    ''' recording the sound '''
    with sr.Microphone() as source:
        recognizer.adjust_for_ambient_noise(source, duration=1)
        recorded_audio = recognizer.listen(source, timeout=6)

    ''' Recorgnizing the Audio '''
    try:
        texte = recognizer.recognize_google(
                recorded_audio, 
                language="fr-FR"
            )
    except:
        texte = 'recommence'

    translation = translator.translate(texte, dest='en')
    resultat = translation.text

    tts = gTTS(resultat, lang='en', slow=True)
    tts.save('letexte.mp3')
    time.sleep(1)
    new = AudioSegment.from_mp3("letexte.mp3")
    new.export('result.ogg', format='ogg')
    time.sleep(1)
    playsound('result.ogg')
    time.sleep(5)
    os.remove('letexte.mp3')
    os.remove('result.ogg')

def en_fr():
    recognizer = sr.Recognizer()
    ''' recording the sound '''
    with sr.Microphone() as source:
        recognizer.adjust_for_ambient_noise(source, duration=1)
        recorded_audio = recognizer.listen(source, timeout=6)

    ''' Recorgnizing the Audio '''
    try:
        texte = recognizer.recognize_google(
                recorded_audio, 
                language="en-GB"
            )
    except:
        texte = 'start again'

    translation = translator.translate(texte, dest='fr')
    resultat = translation.text

    tts = gTTS(resultat, lang='fr', slow=True)
    tts.save('letexte.mp3')
    time.sleep(1)
    new = AudioSegment.from_mp3("letexte.mp3")
    new.export('result.ogg', format='ogg')
    time.sleep(1)
    playsound('result.ogg')
    time.sleep(5)
    os.remove('letexte.mp3')
    os.remove('result.ogg')


def de_en():
    recognizer = sr.Recognizer()
    ''' recording the sound '''
    with sr.Microphone() as source:
        recognizer.adjust_for_ambient_noise(source, duration=1)
        recorded_audio = recognizer.listen(source, timeout=6)

    ''' Recorgnizing the Audio '''
    try:
        texte = recognizer.recognize_google(
                recorded_audio, 
                language="de-DE"
            )
    except:
        texte = 'Fang nochmal an'

    translation = translator.translate(texte, dest='en')
    resultat = translation.text

    tts = gTTS(resultat, lang='en', slow=True)
    tts.save('letexte.mp3')
    time.sleep(1)
    new = AudioSegment.from_mp3("letexte.mp3")
    new.export('result.ogg', format='ogg')
    time.sleep(1)
    playsound('result.ogg')
    time.sleep(5)
    os.remove('letexte.mp3')
    os.remove('result.ogg')

def en_de():
    recognizer = sr.Recognizer()
    ''' recording the sound '''
    with sr.Microphone() as source:
        recognizer.adjust_for_ambient_noise(source, duration=1)
        recorded_audio = recognizer.listen(source, timeout=6)

    ''' Recorgnizing the Audio '''
    try:
        texte = recognizer.recognize_google(
                recorded_audio, 
                language="en-GB"
            )
    except:
        texte = 'start again'

    translation = translator.translate(texte, dest='de')
    resultat = translation.text

    tts = gTTS(resultat, lang='de', slow=True)
    tts.save('letexte.mp3')
    time.sleep(1)
    new = AudioSegment.from_mp3("letexte.mp3")
    new.export('result.ogg', format='ogg')
    time.sleep(1)
    playsound('result.ogg')
    time.sleep(5)
    os.remove('letexte.mp3')
    os.remove('result.ogg')

def esp_en():
    recognizer = sr.Recognizer()
    ''' recording the sound '''
    with sr.Microphone() as source:
        recognizer.adjust_for_ambient_noise(source, duration=1)
        recorded_audio = recognizer.listen(source, timeout=6)

    ''' Recorgnizing the Audio '''
    try:
        texte = recognizer.recognize_google(
                recorded_audio, 
                language="es-ES"
            )
    except:
        texte = 'empezar de nuevo'

    translation = translator.translate(texte, dest='en')
    resultat = translation.text

    tts = gTTS(resultat, lang='en', slow=True)
    tts.save('letexte.mp3')
    time.sleep(1)
    new = AudioSegment.from_mp3("letexte.mp3")
    new.export('result.ogg', format='ogg')
    time.sleep(1)
    playsound('result.ogg')
    time.sleep(5)
    os.remove('letexte.mp3')
    os.remove('result.ogg')

def en_esp():
    recognizer = sr.Recognizer()
    ''' recording the sound '''
    with sr.Microphone() as source:
        recognizer.adjust_for_ambient_noise(source, duration=1)
        recorded_audio = recognizer.listen(source, timeout=6)

    ''' Recorgnizing the Audio '''
    try:
        texte = recognizer.recognize_google(
                recorded_audio, 
                language="en-GB"
            )
    except:
        texte = 'start again'

    translation = translator.translate(texte, dest='es')
    resultat = translation.text

    tts = gTTS(resultat, lang='es', slow=True)
    tts.save('letexte.mp3')
    time.sleep(1)
    new = AudioSegment.from_mp3("letexte.mp3")
    new.export('result.ogg', format='ogg')
    time.sleep(1)
    playsound('result.ogg')
    time.sleep(5)
    os.remove('letexte.mp3')
    os.remove('result.ogg')

def ita_en():
    recognizer = sr.Recognizer()
    ''' recording the sound '''
    with sr.Microphone() as source:
        recognizer.adjust_for_ambient_noise(source, duration=1)
        recorded_audio = recognizer.listen(source, timeout=6)

    ''' Recorgnizing the Audio '''
    try:
        texte = recognizer.recognize_google(
                recorded_audio, 
                language="it-IT"
            )
    except:
        texte = 'ricomincia'

    translation = translator.translate(texte, dest='en')
    resultat = translation.text

    tts = gTTS(resultat, lang='en', slow=True)
    tts.save('letexte.mp3')
    time.sleep(1)
    new = AudioSegment.from_mp3("letexte.mp3")
    new.export('result.ogg', format='ogg')
    time.sleep(1)
    playsound('result.ogg')
    time.sleep(5)
    os.remove('letexte.mp3')
    os.remove('result.ogg')

def en_ita():
    recognizer = sr.Recognizer()
    ''' recording the sound '''
    with sr.Microphone() as source:
        recognizer.adjust_for_ambient_noise(source, duration=1)
        recorded_audio = recognizer.listen(source, timeout=6)

    ''' Recorgnizing the Audio '''
    try:
        texte = recognizer.recognize_google(
                recorded_audio, 
                language="en-GB"
            )
    except:
        texte = 'start again'

    translation = translator.translate(texte, dest='it')
    resultat = translation.text

    tts = gTTS(resultat, lang='it', slow=True)
    tts.save('letexte.mp3')
    time.sleep(1)
    new = AudioSegment.from_mp3("letexte.mp3")
    new.export('result.ogg', format='ogg')
    time.sleep(1)
    playsound('result.ogg')
    time.sleep(5)
    os.remove('letexte.mp3')
    os.remove('result.ogg')

label1 = Label(frame, text='press a button, wait 1sec.\n talk for a maximum of 8 sec.\n wait a bit for translate.\n wait for the button to return \nto his place to make\n a new choice')
label1.grid(row=2, column=4)

but1 = Button(root, text="esp => en", padx=33, pady=20, borderwidth=2, relief="raised", command=esp_en)
but2 = Button(root, text="en => esp", padx=33, pady=20, borderwidth=2, relief="raised",  command=en_esp)
but3 = Button(root, text="ita => en", padx=36, pady=20, borderwidth=2, relief="raised",  command=ita_en)
but4 = Button(root, text="de => en", padx=37, pady=20, borderwidth=2, relief="raised",  command=de_en)
but5 = Button(root, text="en => de", padx=37, pady=20, borderwidth=2, relief="raised",  command=en_de)
but6 = Button(root, text="en => ita", padx=36, pady=20, borderwidth=2, relief="raised",  command=en_ita)
but7 = Button(root, text="fr => en", padx=40, pady=20, borderwidth=2, relief="raised",  command=fr_en)
but8 = Button(root, text="en => fr", padx=40, pady=20, borderwidth=2, relief="raised",  command=en_fr)

but1.grid(row=3, column=0)
but2.grid(row=3, column=1)
but3.grid(row=4, column=0)
but4.grid(row=2, column=0)
but5.grid(row=2, column=1)
but6.grid(row=4, column=1)
but7.grid(row=1, column=0)
but8.grid(row=1, column=1)

if __name__ == "__main__":
    root.mainloop()
