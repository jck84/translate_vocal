from requests_html import HTMLSession
import speech_recognition as sr
import datetime
import time
import pywhatkit
import wikipedia
import pyjokes
from gtts import gTTS
from pydub import AudioSegment
import os
from playsound import playsound
from googletrans import Translator
import random
import webbrowser
import requests

#init scrapper
session = HTMLSession()
#init traducteur
translator = Translator()
wikipedia.set_lang("fr")

#liste des réponses aléatoires
liste1 = ["oui monsieur ?", "je vous ecoute", "je suis a votre service", "dite moi tout", "encore du travail ? ", "oui ?", "que voulez vous?", "je peux vous aider?"]
liste2 = ["a la prochaine", "a bientot", "salut", "au revoir"]
liste3 = ["je n'ai pas compris", "je ne comprend pas", "articulez et essayer a nouveau", "tu parle dans ta barbe, essai encore"]

#pour faire parler
def parle(blabla):
    tts = gTTS(blabla, lang='fr', slow=False)
    tts.save('letexte.mp3')
    new = AudioSegment.from_mp3("letexte.mp3")
    new.export('result.ogg', format='ogg')
    playsound('result.ogg')
    time.sleep(0.5)
    os.remove('letexte.mp3')
    os.remove('result.ogg')
    time.sleep(0.5)

#prise en compte de la voix utilisateur (dire andouille a la place d'alexa pour demarrer l'enregistrement')
def user_commands():
    while True:
        recognizer = sr.Recognizer()
        print("vous pouvez parler...")
        ''' recording the sound '''
        with sr.Microphone() as source:
            recognizer.adjust_for_ambient_noise(source, duration=1)
            recorded_audio = recognizer.listen(source, timeout=99999)
        ''' Recorgnizing the Audio '''
        try:
            texte = recognizer.recognize_google(
                    recorded_audio, 
                    language="fr-FR")
            if "andouille" in texte:
                texte = texte.replace('andouille', '')
                return texte
            else:
                texte = ''
                continue
        except:
            texte = 'neutrina'
            
#conversion simplifié kelvin to degrés
def kel_to_cel(kelvin):
    base = 273
    temp = (int(kelvin) - base)
    return temp

#request de l'api pour la météo openweather map
def meteo(ville):
    api_key = "04606a6c20cdb2bd4bdc08246aecead7"
    url_base = "https://api.openweathermap.org/data/2.5/weather?"
    city_nom = ville
    complete_url = url_base + "appid=" + api_key + "&q=" + city_nom
    reponse = requests.get(complete_url)
    x = reponse.json()
    if x["cod"] != "404":
        y = x["main"]
        temp = y["temp"]
        pressure = y["pressure"]
        humid = y["humidity"]
        z = x["weather"]
        liste4 = []
        liste4.append(temp)
        liste4.append(pressure)
        liste4.append(humid)
        return liste4

#fonction principale
def run_alexa():
    parle(random.choice(liste1))
    texte = user_commands()
    if 'musique' in texte:
        song = texte.replace('musique', '')
        pywhatkit.playonyt(song)
        talk = ('je joue la musique ' + song)
        parle(talk)
        time.sleep(2)
    elif 'information' in texte:
        r = session.get('https://www.francetvinfo.fr/')
        about = r.html.find('html.no-js body main section.home div.col-2 div.col-right ul')
        info = about[0].text
        print(info)
        talk = ("voici quelques informations du jour :   " + info)
        parle(talk)
        time.sleep(2)
    elif 'heure' in texte:
        heure = datetime.datetime.now().strftime('%I:%M %p')
        talk = ('il est ' + heure)
        tts = gTTS(talk, lang='fr', slow=False)
        parle(talk)
        time.sleep(2)
    elif 'jour' in texte:
        t = datetime.date.today()
        p = list(str(t))
        jour = p[8] + p[9]
        mois = p[5] + p[6]
        annee = p[0] + p[1] + p[2]+ p[3]
        datelettre = jour + ' ' + mois + ' ' + annee
        talk = ('on est le ' + datelettre)
        parle(talk)
        time.sleep(2)
    elif 'météo' in texte:
        meteo1 = meteo("grenoble")
        temperature = kel_to_cel(meteo1[0])
        talk = ("A grenoble ,il fait... " + str(temperature)+ " degrés..." + ", la pression atmosphérique est : " + str(meteo1[1]) + " hecto pascal" +", l'humidité est de : " + str(meteo1[2]) + " pour cent")
        parle(talk)
        time.sleep(1)
        r = session.get('https://www.lachainemeteo.com/meteo-france/previsions-meteo-france-aujourdhui')
        about = r.html.xpath('//*[@id="weatherSituation"]')
        webbrowser.open("https://meteofrance.com/",new=2)
        parle(about[0].text)
        time.sleep(2)
    elif "programme télé" in texte:
         webbrowser.open("https://www.programme-tv.net/",new=2)
         time.sleep(2) 
    elif 'qui est ' in texte:
        person = texte.replace('qui est ', '')
        print(person)
        try:
            info = wikipedia.summary(person, 1)
            print(info)
            parle(info)
        except:
            parle(random.choice(liste3))           
        time.sleep(2)
    elif "c'est quoi " in texte:
        information = texte.replace("c'est quoi ", '')
        try:       
            info1 = wikipedia.summary(information, sentences=1)
            print(info1)
            parle(info1)
            time.sleep(2)      
        except:
            parle(random.choice(liste3)) 
            time.sleep(2)
    elif 'au revoir' in texte:
        parle(random.choice(liste2))
        time.sleep(2)
        quit()
    elif 'blague' in texte:
        talk = (pyjokes.get_joke('en', 'all'))
        translation = translator.translate(talk, dest='fr')
        resultat = translation.text
        print(resultat)
        parle(resultat)
        time.sleep(2)
    else:
        parle(random.choice(liste3)) 
        time.sleep(2)

if __name__ == "__main__":
    parle("Bonjour, je suis une andouille... ")
    while True:
        run_alexa()